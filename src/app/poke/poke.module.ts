import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PokeRoutingModule } from './poke-routing.module';
import { ListComponent } from './components/list/list.component';
import { DetailComponent } from './components/detail/detail.component';
import { CardComponent } from './components/list/card/card.component';

@NgModule({
  declarations: [ListComponent, DetailComponent, CardComponent],
  imports: [CommonModule, PokeRoutingModule],
})
export class PokeModule {}
