import { Component, OnInit, Self } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { PokeApiService } from '../../services/pokeApi/poke-api.service';

@UntilDestroy({ checkProperties: true })
@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
  providers: [PokeApiService],
})
export class DetailComponent implements OnInit {
  pokeData: any;

  constructor(
    private _activatedRoute: ActivatedRoute,
    @Self() private _pokeApi: PokeApiService
  ) {}

  ngOnInit(): void {
    this.getRoutedata();
  }

  // Get Pokemon info for specific id
  private getThePokemon(res: any): void {
    this._pokeApi
      .getPokemonData(res.id)
      .pipe(untilDestroyed(this))
      .subscribe((res) => {
        this.pokeData = res;
        console.log(this.pokeData);
      });
  }

  // Getting dynamic route data
  private getRoutedata(): void {
    this._activatedRoute.params.subscribe((res) => this.getThePokemon(res));
  }
}
