import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DefaultViewComponent } from './views/default-view/default-view.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SubNavComponent } from './components/sub-nav/sub-nav.component';
import { StarWarsModule } from './star-wars/star-wars.module';

@NgModule({
  declarations: [
    AppComponent,
    DefaultViewComponent,
    NavbarComponent,
    SubNavComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule],
  bootstrap: [AppComponent],
})
export class AppModule {}
