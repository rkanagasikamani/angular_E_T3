import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SwapiService } from '../../services/swapi/swapi.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit {
  public characterDetails: any;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _swapi: SwapiService
  ) {}

  ngOnInit(): void {
    this.getId();
  }

  // Get dynamic data from route
  private getId(): void {
    this._activatedRoute.params.subscribe((res) =>
      this.getcharacterDetails(res)
    );
  }

  // Get Character details from service via id
  private getcharacterDetails(params: any): void {
    this._swapi.getCharacterDetails(params.id).subscribe((res) => {
      this.characterDetails = res;
      console.log(this.characterDetails);
    });
  }
}
